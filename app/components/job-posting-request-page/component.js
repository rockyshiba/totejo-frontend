import Vue from "app/vue";
import Constants from "app/constants";
import Util from "mrman/vue-component-library/util";

import AlertService from "app/services/alert";
import UserService from "app/services/user";
import IndustryService from "app/services/industry";
import JobsService from "app/services/jobs";
import AppService from "app/services/app";

import "mrman/vue-component-library/components/alert-notification/component";
import "mrman/vue-component-library/components/alert-listing/component";

import t from "./template.html!vtc";
import "./style.css!";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    appSvc: { type: Object, default: () => AppService },
    userSvc: { type: Object, default: () => UserService },
    alertSvc: { type: Object, default: () => AlertService },
    industrySvc: { type: Object, default: () => IndustryService },
    jobsSvc: { type: Object, default: () => JobsService }
  },

  data: function() {
    return {
      alerts: this.alertSvc.getAlertsForNamespace("job-posting-request-page"),

      availableJobTypes: Util.makeOptionsFromEnumTranslation(Constants.ENUM_TRANSLATIONS.job.type),
      availableCurrencies: Util.makeOptionsFromEnumTranslation(Constants.ENUM_TRANSLATIONS.currency),

      submittedOnceWithErrors: false,
      submittedJobPostingRequest: false,
      submissionErrored: false,

      inquiryEmailAddress: this.appSvc.state.inquiryEmailAddress,

      enumTranslations: Constants.ENUM_TRANSLATIONS,

      email: "",
      title: "",
      desc: " ", // Stop storing descriptions, empty string for now
      selectedIndustry: "IT",
      selectedJobType: "FullTime",
      minSalary: null,
      maxSalary: null,
      currency: null,
      applyLink: "",

      mailHref: `mailto:${this.appSvc.state.inquiryEmailAddress}?title=Tell me more about your services!`
    };
  },

  computed: {

    emailIsValid: function() { return this.email.trim().length > 0; },
    titleIsValid: function() { return this.title.trim().length > 0; },
    applyLinkIsValid: function() { return this.applyLink.trim().length > 0; },

    selectedIndustryIsValid: function() { return this.selectedIndustry !== ""; },
    selectedJobTypeIsValid: function() { return this.selectedJobType !== ""; }
  },

  methods: {

    validateForm: function() {
      return [this.emailIsValid,
        this.titleIsValid,
        this.selectedIndustryIsValid,
        this.selectedJobTypeIsValid,
        this.applyLinkIsValid
      ].every(v => v);
    },

    requestJobPosting: function() {
      if (!this.validateForm()) {
        this.submittedOnceWithErrors = true;
        return;
      }

      // Create the job posting request to send to the backend
      var req = {
        rTitle: this.title,
        rDescription: this.desc,
        rIndustry: this.selectedIndustry,
        rJobType: this.selectedJobType,
        rPosterEmail: this.email,
        rApplyLink: this.applyLink,
        rRequestedAt: new Date() // will be replaced server-side
      };

      // If the user is logged in, add possibly modified salary info currency
      if (this.userSvc.state.userIsLoggedIn) {
        if (this.minSalary) { req.rMinSalary = this.minSalary; }
        if (this.maxSalary) { req.rMaxSalary = this.maxSalary; }
        if (this.currency) { req.rSalaryCurrency = this.currency; }
      }

      this.jobsSvc.addPostingRequest(req)
        .then(() => { this.submittedJobPostingRequest = true; },
          () => { this.submittedJobPostingRequest = true; this.submissionErrored = true; });

    }
  }
};

// RSegister and export component
Vue.component("job-posting-request-page", comp);
export default comp;
