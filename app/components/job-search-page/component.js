import _ from "lodash";
import Vue from "app/vue";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";

import AppService from "app/services/app";
import UserService from "app/services/user";
import AlertService from "app/services/alert";
import JobsService from "app/services/jobs";
import TagsService from "app/services/tags";
import IndustryService from "app/services/industry";
import MailingListService from "app/services/mailing-list";

import JobWithCompany from "app/models/job-with-company";

import "app/components/job-search-box/component";
import "app/components/mailing-list-signup/component";
import "mrman/vue-component-library/components/job-listing-item/component";
import "mrman/vue-component-library/components/alert-listing/component";
import "mrman/vue-component-library/components/generic-listing/component";

import t from "./template.html!vtc";
import "./style.css!";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    appSvc: {type: Object, default: () => AppService},
    userSvc: {type: Object, default: () => UserService},
    alertSvc: {type: Object, default: () => AlertService},
    jobsSvc: {type: Object, default: () => JobsService},
    industrySvc: {type: Object, default: () => IndustryService},
    tagsSvc: {type: Object, default: () => TagsService},
    mailingListSvc: {type: Object, default: () => MailingListService},

    searchTerm: {type: String, default: ""},
    industries: [String, Array],
    tags: [String, Array],
    companies: [String, Array]
  },

  data: function() {
    const industries = _.isString(this.industries) ? [this.industries] : this.industries;
    const companies = _.isString(this.companies) ? [this.companies] : this.companies;
    const tags = _.isString(this.tags) ? [this.tags] : this.tags;

    const initialSearchQuery = {
      searchTerm: this.searchTerm,
      companies: companies,
      industries: industries,
      tags: tags
    };

    return {
      jobAlerts: this.alertSvc.getAlertsForNamespace("job-search-page"),

      jobListingCmdBus: new Vue(),
      jobClass: JobWithCompany,

      jobsUrl: Util.makeAPIUrl(Constants.URLS.SERVER_BASE_URL, Constants.URLS.API_BASE, Constants.URLS.API_VERSION, Constants.URLS.JOBS_SEARCH),
      jobsListingGetQueryAugmentFn: q => {
        // Add the industries/companies to the query
        if (!Util.isNullOrUndefined(this.currentSearchQuery.searchterm)) { _.extend(q, {searchTerm: this.currentSearchQuery.searchTerm}); }
        if (!Util.isNullOrUndefined(this.currentSearchQuery.industries)) { _.extend(q, {industries: this.currentSearchQuery.industries}); }
        if (!Util.isNullOrUndefined(this.currentSearchQuery.companies)) { _.extend(q, {companies: this.currentSearchQuery.companies}); }
        if (!Util.isNullOrUndefined(this.currentSearchQuery.tags)) { _.extend(q, {tags: this.currentSearchQuery.tags}); }
        return q;
      },

      // Ensure the industries always comes out as a list
      industryList: industries,

      tagList: tags,
      availableTags: [],

      companyList: [],
      currentSearchQuery: initialSearchQuery || {},
      enumTranslations: Constants.ENUM_TRANSLATIONS
    };
  },

  methods: {
    // Update the URL for the page with a given search query
    updateUrl: function(query) {
      this.$router.replace(this.$route.path + Util.makeJobSearchQueryString(query));
    },

    doSearch: function() {
      this.jobListingCmdBus.$emit("do-search");
    },

    handleUpdatedSearchQuery: function(query) {
      this.currentSearchQuery = query;
      this.updateUrl(this.currentSearchQuery);
      this.doSearch();
    }

  }
};

// Register and export component
Vue.component("job-search-page", comp);
export default comp;
