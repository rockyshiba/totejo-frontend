import _ from "lodash";
import Vue from "app/vue";

import t from "./template.html!vtc";
import "./style.css!";

import "mrman/vue-component-library/components/tag-toggle-selector/component";
import "mrman/vue-component-library/directives/focus";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    initialSearchTerm: String,
    availableIndustrySet: {type: Array, required: true},
    initialSelectedIndustries: Array,
    initialSelectedTagNames: Array,

    tagsSvc: {type: Object, required: true},

    showIndustriesSelector: {type: Boolean, default: false},
    showTagSelector: {type: Boolean, default: false},
    showTextSearch: {type: Boolean, default: true}
  },

  data: function() {

    // Retrieve and fill in the available tags
    if (this.tagsSvc) {
      this.tagsSvc
        .getOrFetchAllTags()
        .then(tags => this.availableTags = tags);
    }

    return {
      searchTerm: this.initialSearchTerm || "",
      selectedIndustries: this.initialSelectedIndustries || [],
      selectedTagNames: this.initialSelectedTagNames || [],
      selectedTagName: null,

      availableTags: [] // will contain the total list of full tag objects that are available
    };

  },

  watch: {
    searchQuery: function() { this.$emit("search-query-update", this.searchQuery); }
  },

  computed: {
    availableIndustries: function() {
      return this.availableIndustrySet ? [...this.availableIndustrySet] : [];
    },

    industryTags: function() {
      return _.map(this.availableIndustries, i => ({name: i, value: i}));
    },

    noIndustriesSelected: function() { return this.selectedIndustries.isEmpty(); },

    availableTagNames: function() { return this.availableTags.map(t => t.name); },

    unusedTagNames: function() { return _.without(this.availableTagNames, this.selectedTagNames); },

    selectedTags: function() {
      return this.selectedTagNames
        .map(name => this.availableTags.find(t => name === t.name) || {name:name, cssColor: "rgba(120,120,120,0.4)"})
        .filter(t => t);
    },

    searchQuery: function() {
      let query = {};
      if (this.searchTerm) { query.searchTerm = this.searchTerm; }
      if (this.selectedIndustries && this.selectedIndustries.length > 0) { query.industries = this.selectedIndustries || []; }
      if (this.selectedTagNames) { query.tags = this.selectedTagNames; }
      if (this.selectedTags) { query.tagObjects = this.selectedTags; }
      return query;
    }
  },

  methods: {

    clearTags: function() {
      this.selectedTagNames.splice(0, this.selectedTagNames.length);
      this.$emit("search", this.searchQuery);
    },

    // $emit search
    triggerSearch: function() {
      this.$emit("search", this.searchQuery);
    },

    // Handle industry selections updating
    industryTagsChanged: function(industries) {
      this.selectedIndustries = industries;
    },

    addSelectedTagName: function() {
      // Exit early if the tag name is null, empty or if it's already present
      if (_.isNull(this.selectedTagName) || _.isEmpty(this.selectedTagName.trim()) || this.selectedTagNames.includes(this.selectedTagName)) {
        return;
      }

      this.selectedTagNames.push(this.selectedTagName.trim());
      this.selectedTagName = "";
    },

    removeSelectedTagByName: function(tagName) {
      let idx = this.selectedTagNames.findIndex(tname => tname === tagName);
      if (idx >= 0) {
        this.selectedTagNames.splice(idx, 1);
      }
    }

  }

};

// Register as global component & export
Vue.component("job-search-box", comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };
