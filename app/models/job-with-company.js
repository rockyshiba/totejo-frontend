import Job from "app/models/job";
import Company from "app/models/company";

class JobWithCompany {
  constructor(data={}) {
    this.job = new Job(data.job);
    this.company = new Company(data.company);
  }
}

export default JobWithCompany;
