/**
 * Model to  smoothe over CompanySessions/CompanyInfo/CompanyWithoutSensitiveData objects from backend
 * @class
 */
class Company {
  constructor(data={}) {
    this.id = data.id;
    this.name = data.companyName || data.name;
    this.description = data.companyDescription || data.description;
    this.cultureDescription = data.companyCultureDescription || data.cultureDescription;
    this.homepageUrl = data.companyHomepageURL || data.homepageUrl;
    this.iconUri = data.companyIconURI || data.iconUri;
    this.bannerImageUri = data.companyBannerImageURI || data.bannerImageUri;
    this.primaryColorCSS = data.companyPrimaryColorCSS || data.primaryColorCSS;
    this.createdAt = data.companyCreatedAt || data.createdAt;
    this.lastUpdatedAt = data.companyLastUpdatedAt || data.lastUpdatedAt;
    this.addressId = data.companyAddresID || data.addressId;
  }

  serialize() {
    return {
      companyName: this.name,
      companyDescription: this.description,
      companyCultureDescription: this.cultureDescription,
      companyHomepageURL: this.homepageUrl,
      companyIconURI: this.iconUri,
      companyBannerImageURI: this.bannerImageUri,
      companyAddressId: this.addressId,
      companyPrimaryColorCSS: this.primaryColorCSS,
      companyCreatedAt: this.createdAt,
      companyLastUpdatedAt: this.lastUpdatedAt,
      addressId: this.addressId
    };
  }
}

export default Company;
