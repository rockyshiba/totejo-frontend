import Vue from "app/vue";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";

/**
 * Performs backend functionality related to mailing list
 */
var MailingListService = new Vue({

  methods: {

    /**
     * Subscribe to the mailing list
     *
     * @param {string} email - email address of the user
     * @param {number[]} tagIds - tag names that the user is interested in
     * @returns A promise that resolves when the user has been added to the mailing list
     */
    subscribe: function(email, tagIds) {
      let url = `${Constants.URLS.API_BASE_URL}/${Constants.URLS.SUBSCRIPTIONS}/email?email=${email}`;

      if (tagIds && tagIds.length > 0) {
        url += "&" + tagIds.map(t => `tagIds[]=${t}`).join("&");
      }

      return Util.postToAPI(url)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(({respData}) => respData);
    },

    /**
     * Retrieve information for a given mailing list token
     *
     * @param {string} token
     * @returns a Promise that resolves to the information about the mailing list subscription
     */
    confirmSubscription: function(token) {

      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.SUBSCRIPTIONS}/email/confirm?token=${token}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(({respData}) => respData);
    },

    /**
     * Retrieve information for a given mailing list token
     *
     * @param {string} token
     * @returns a Promise that resolves to the information about the mailing list subscription
     */
    unsubscribe: function(token) {

      return Util.postToAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.SUBSCRIPTIONS}/email/unsubscribe?token=${token}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(({respData}) => respData);
    }

  }

});

export default MailingListService;
