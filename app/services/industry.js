import _ from "lodash";
import Vue from "app/vue";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";


var IndustryService = new Vue({
  data: {
    state: {
      industries: []
    }
  },

  created: function() {
    // Attempt to fetch all industries when the service is created
    this.fetchIndustries();
  },

  methods: {

    getIndustries: function() { return _.get(this, "state.industries"); },

    /**
     * Get all industries, updating service internal data.
     *
     * @returns A promise that resolves to a list of javascript set of all industrys available
     */
    fetchIndustries: function() {
      return new Promise((resolve, reject) => {
        Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.INDUSTRIES}`)
          .then(Util.jsonifyFetchResponse)
          .then(({status, respData}) => {

            // Ensure each industry is in the internal hash
            if (status == "success") {
              var dataset = _.reduce(respData, (acc,v) => acc.add(v), new Set());
              this.state.industries = [...dataset];
            }

            resolve(this.state.industries);
          })
          .catch(reject);
      });
    }

  }
});

export default IndustryService;
