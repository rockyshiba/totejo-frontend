import Vue from "app/vue";
import _ from "lodash";
import Util from "mrman/vue-component-library/util";
import Constants from "app/constants";

const SUBROUTES = {
  ADMIN: ["/app/admin"],
  COMPANY_ADMIN: ["/app/company-admin"]
};

var window = window || window; // trick SystemJS into thinking window is OK

var AppInfoService = new Vue({
  data: function() {
    var defaultDomain = window ? window.location.hostname : "techjobs.tokyo";

    return {
      state: {
        appName: "Job Board Admin",
        appDescription: "Job board administration portal",
        appDomain: defaultDomain,
        adminURL: `//admin.${defaultDomain}`,
        inquiryEmailAddress: `hello@${defaultDomain}`,
        supportEmailAddress: `support@${defaultDomain}`,
        pageLoadRoute: "",
        router: null
      }
    };
  },

  methods: {

    // Fetch app statistics
    fetchAdminDashboardStatistics:  function() {
      return Util.getFromAPI(`${Constants.URLS.API_BASE_URL}/${Constants.URLS.ADMIN_STATISTICS}`)
        .then(Util.errorOnStatusOtherThan([200]))
        .then(Util.jsonifyFetchResponse)
        .then(_.partialRight(_.get, "respData"));
    },

    /**
     * Set the current app's router
     *
     * @param {Object} router - The (global) router to save
     */
    setRouter: function(router) {
      // Ensure email/password are provided
      if (_.isUndefined(router) || _.isNull(router)) {
        throw new Error("Invalid router object");
      }

      this.state.router = router;
    },

    /**
     * Set this service's record of the first route the app tried to go to
     *
     * @param {string} hash - The first route the app tried to go to
     */
    setPageLoadRoute: function(hash) {
      this.state.pageLoadRoute = hash;
    },

    /**
     * Check if a given route name/path is in the list of routes traveled to get where the user is
     *
     * @param {string} nameOrPath - The name/path of the route
     * @returns whether the given name or path has been traveled
     */
    routesWereTraveled: function(namesOrPaths) {
      return this.state.router.history.current.matched
        .some(r => _.some(namesOrPaths, s => s === r.path || s === r.name));
    },

    /**
     * Detects whether the application in currently in a given subroute
     *
     * @param {string} subroute - Name of the subroute to check for (looked up SUBROUTES)
     * @return Whether the app is in a given subroute or any subroute at all
     */
    inSubRoute: function(subroute) {
      // If no subroute was provided, check if
      if (_.isUndefined(subroute)) {
        return _.some(_.values(SUBROUTES), rs => this.routesWereTraveled(rs));
      }

      if (!(subroute in SUBROUTES)) { throw new Error(`Invalid sub route [${subroute}]`); }
      return this.routesWereTraveled(SUBROUTES[subroute]);
    },

    /**
     * Utility function to nicely perform an action on the router (or fail if it doesn't exist)
     *
     * @param {Function} f - The function to perform on the router
     * @returns The result of executing the function on the router if there is one, null otherwise
     */
    doNavigation: function(f) {
      if (this.state.router) {
        return f.call(this, this.state.router);
      }
      return null;
    },

    /**
     * Convenience function for redirecting to login
     *
     * @param {object} opts - options
     * @param {boolean} opts.noRedirect - Don't attempt to redirect to where they came from
     */
    redirectToLogin: function(opts={}) {

      var query = {};
      if (!opts.noRedirect) {
        query.redirectUrl = opts.usePageLoadRoute ? this.state.pageLoadRoute : this.state.router.history.current.fullPath;
      }

      this.doNavigation(r => r.push({name: "login", query: query}));
    },

    /**
     * Notify all listening components that the global submenu nav should/will be shown
     */
    notifyShowSubmenu: function() { this.$emit("show-submenu"); },

    getAppName: function() { return _.get(this, "state.appName", ""); }
  }
});

export default AppInfoService;
