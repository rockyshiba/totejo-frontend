#!/usr/bin/env node

var cp = require('child_process');
console.log("INFO: Installing jspm [local] registry...");
var childProcess = cp.spawn('./node_modules/.bin/jspm', ['registry', 'create', 'local', 'jspm-local']);

childProcess.stdout.setEncoding('utf8');

childProcess.stdout.on("data", function(data) {
  if (/^Registry local already exists/.exec(data)) {
    childProcess.stdin.write('n\n');
    childProcess.stdin.end();
  }
});
