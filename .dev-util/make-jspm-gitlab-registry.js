#!/usr/bin/env node

var cp = require('child_process');
var childProcess = cp.spawn('./node_modules/.bin/jspm', ['registry', 'create', 'gitlab', 'jspm-git']);

childProcess.stdout.setEncoding('utf8');

childProcess.stdout.on("data", function(data) {
  console.log(`DATA: ${data}`);
  if (/^Enter the base URL/.exec(data)) {
    childProcess.stdin.write( 'https://gitlab.com' + '\n');
  }

  if (/^Set advanced configurations?/.exec(data)) {
    childProcess.stdin.write('no\n');
    childProcess.stdin.end();
  }

  if (/^Registry gitlab already exists/.exec(data)) {
    childProcess.stdin.write('n\n');
    childProcess.stdin.end();
  }

});
