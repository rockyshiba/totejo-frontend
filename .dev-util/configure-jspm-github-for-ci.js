#!/usr/bin/env node
/* global process */

if (!process.env.GITHUB_USERNAME) { throw new Error("GITHUB_USERNAME is missing"); }
if (!process.env.GITHUB_PERSONAL_ACCESS_TOKEN) { throw new Error("GITHUB_PERSONAL_ACCESS_TOKEN is missing"); }

var cp = require('child_process');
console.log("INFO: Configuring [github] registry for use by JSPM...");
var childProcess = cp.spawn('./node_modules/.bin/jspm', ['registry', 'config', 'github']);

childProcess.stdout.setEncoding('utf8');

childProcess.stdout.on("data", function(data) {
  if (/Would you like to set up your GitHub/.exec(data)) {
    childProcess.stdin.write("yes\n");
  }

  if (/Enter your GitHub username/.exec(data)) {
    console.log("Setting github username...");
    childProcess.stdin.write(`${process.env.GITHUB_USERNAME}\n`);
  }

  if (/Enter your GitHub password or access token/.exec(data)) {
    console.log("entering access token...");
    childProcess.stdin.write(`${process.env.GITHUB_PERSONAL_ACCESS_TOKEN}\n`);
  }

  if (/Would you like to test/.exec(data)) {
    console.log("answering yes to test...");
    childProcess.stdin.write("yes\n");
    childProcess.stdin.end();
  }
});
